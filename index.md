---
layout: default
title: About - Tobias Schulz
---

# Hello!

This is the personal website of Tobias Schulz. I'm a computer science student at the [![KIT]({{site.baseurl}}img/kit.ico "KIT") Karlsruher Institute of Technology](http://www.informatik.kit.edu/) (KIT) in Germany. I was born in Frankfurt am Main in 1993 and grew up in Bad Homburg. I got my Abitur (German qualification for university entrance) in June 2011 from the [Anna Schmidt Schule](http://www.anna-schmidt-schule.de) in Frankfurt am Main.

<!--
If you want to learn more about the open source projects I'm involved in, [click here](projects.html).
-->

<!--
Since 2006 I'm developing the [FreeHAL Project](http://en.wikipedia.org/wiki/FreeHAL), an artificial intelligence software which very closely imitates human conversation. If you are looking for information about FreeHAL, please go to [Github](https://github.com/freehal) or [Wikipedia](http://en.wikipedia.org/wiki/FreeHAL). For the BOINC project FreeHAL@home, please refer to [Boincstats](http://boincstats.com/en/stats/78/project/detail) or visit the [SETI.Germany forum](http://www.seti-germany.de/forum/freehal/3041-serverstatus-fragen-usw-82.html#post215354) or the [Planet 3DNow! forum](http://www.planet3dnow.de/vbulletin/showthread.php?p=4589871#post4589871).
-->

<!--
<nobr>
<a class="a_img" title="Facebook: tobias.schulz1" href="https://www.facebook.com/tobias.schulz1"><img src="img/icon-facebook.png" style="width: 48px; height: 48px;" /></a>
&nbsp;
<a class="a_img" title="Google+: tobiasschulz1" href="https://plus.google.com/+TobiasSchulz1/posts?hl=de"><img src="img/icon-googleplus.png" style="width: 48px; height: 48px;" /></a>
&nbsp;
<a class="a_img" title="Github: tobiasschulz" href="https://github.com/tobiasschulz"><img src="img/icon-github.png" style="width: 48px; height: 48px;" /></a>
</nobr>
-->
