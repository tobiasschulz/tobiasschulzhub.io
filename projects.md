---
layout: default
title: Software Projects - Tobias Schulz
---

# Software Projects

<iframe src="https://githubbadge.appspot.com/tobiasschulz" style="border: 0;height: 142px;width: 200px;overflow: hidden; float: right;" frameBorder="0"></iframe>

## FreeHAL

The FreeHAL project's mission is to develop an open source computer program which very closely imitates human conversation. FreeHAL consists of a server application and several frontends, a cross-platform GUI and a web interface.

After more than 6 years of (mostly) active development, the project has been discontinued in 2012.

Website: <http://freehal.github.io/>

Github: @freehal

## Knot3

Knot&sup3; is a student project that I was involved in during the winter term of 2013 at the
[Praxis der Softwareentwicklung](http://cg.ivd.kit.edu/lehre/ws2013/pse/index.php) course at [KIT](http://www.informatik.kit.edu/).
It is available under a free software license.

Website: <http://knot3.github.io/>

Github: @knot3

## MonoGame / SDL#

In early 2014 is was involved in the development of [MonoGame-SDL2](https://github.com/tobiasschulz/MonoGame-SDL2) and I also experimented with integrating [GLSL shader support](https://github.com/tobiasschulz/MonoGame-GLSL) into the code base.

Github: [https://github.com/tobiasschulz/MonoGame-SDL2](tobiasschulz/MonoGame-SDL2)

## Limelight

Since 2013 I created several patches for the limelight gamestream client which implemented proper GL rendering support and custom resolutions. Limelight which is compatible with NVidia's [Geforce Experience](http://www.geforce.com/geforce-experience).

Github: [https://github.com/tobiasschulz/limelight-pc-opengl](tobiasschulz/limelight-pc-opengl)
